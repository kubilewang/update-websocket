# updatewebsocket



#### 架构图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0123/163635_6f336f08_420908.png "20210123163339292.png")

#### 项目介绍
在以前开源的代码上改造而来，没有引入开源的jar包，而是直接把代码集成进来

#### 软件架构
基于websocket和SpringBoot

### 完善框架
1、框架目前存在的问题，MemWebSocketManager和RedisWebSocketManager 不能切换

2、不支持同一用户多点登录都能收到通知

### 修复框架

1、不能切换的问题通过@ConditionalOnProperty这个注解动态控制加载bean

2、同一用户多点登录都能收到通知的解决方案为（登录账号+随机数为确定一个websocket连接）,账号+随机数为一个新的websocket连接。

### 欢迎打赏

<table>

<tr>
   <td><img src="https://images.gitee.com/uploads/images/2021/0125/102009_4d45be8f_420908.png"/></td>
   <td><img src="https://images.gitee.com/uploads/images/2021/0125/102023_72ac0278_420908.png"/></td>
</tr>

